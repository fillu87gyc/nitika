#include "pgmlib.h"
#include <math.h>
#include <stdio.h>

int hist_arry[256]; /* 1d array for histogram*/

void calc_histogram(int n)
{
  int i, x, y;

  for (i = 0; i < 256; i++)
    hist_arry[i] = 0;
  for (y = 0; y < height[n]; y++)
    for (x = 0; x < width[n]; x++)
      hist_arry[image[n][x][y]]++;
}
double *make_relative_frequency_table(int n, double table[])
{
  calc_histogram(n);    // Nf(度数)
  int Nf[256];
  static double pf[256];
  int i;
  int N = 0;    // N=画素数の合計
  for (i = 0; i < 256; i++)
  {
    Nf[i] = hist_arry[i];
    N += hist_arry[i];
  }
  for (i = 0; i < 256; i++)
    pf[i] = ( double )Nf[i] / ( double )N;
  //後ろの値がないので０だけ
  table[0] = pf[0];
  for (i = 1; i < 256; i++)
  {
    table[i] = pf[i] + table[i - 1];
  }
  return table;
}
double *make_binary_table(double threshold, double table[])
{
  int threshold_index;
  int i;
  for (i = 0; table[i] < threshold; i++)
    table[i] = 0;
  threshold_index = i;
  printf("%d,%f\n", threshold_index, table[threshold_index]);
  for (i = threshold_index; i < 256; i++)
    table[i] = 255;
  return table;
}
void make_binary_image(int n1, int n2, double table[])
{
  width[n2]  = width[n1];
  height[n2] = height[n1];
  for (int x = 0; x < width[n2]; x++)
    for (int y = 0; y < height[n2]; y++)
    {
      image[n2][x][y] = table[image[n1][x][y]];
    }
}

int main(void)
{
  double table[256];
  load_image(0, "./textdata.pgm"); /* load image into No.0 */
  make_relative_frequency_table(0, table);
  make_binary_table(0.2, table);
  make_binary_image(0, 1, table);
  save_image(1, "./textdata_b.pgm");
  return 0;
}
